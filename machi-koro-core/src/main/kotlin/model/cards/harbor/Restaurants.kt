package model.cards.harbor

import model.buildings.effects.establishmentEffects.RestaurantStandardEffect
import model.buildings.effects.establishmentEffects.requiring
import model.buildings.establishments.Restaurant

val sushiBar = Restaurant(
    "Sushi Bar",
    "",
    2,
    listOf(1),
    RestaurantStandardEffect(
        3,
        "If you have a harbor, you get 3 coins from the player who rolled the dice."
    ) requiring harbor
)

val pizzaJoint: Restaurant = Restaurant(
    "Pizza Joint",
    "",
    1,
    listOf(7),
    RestaurantStandardEffect(1)
)

val hamburgerStand: Restaurant = Restaurant(
    "Hamburger Stand",
    "",
    1,
    listOf(8),
    RestaurantStandardEffect(1)
)