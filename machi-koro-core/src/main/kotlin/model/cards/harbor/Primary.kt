package model.cards.harbor

import model.asCoins
import model.buildings.PrimaryIndustryType
import model.buildings.effects.establishmentEffects.*
import model.buildings.establishments.PrimaryIndustry
import model.gameEntities.DiceCount
import model.gameEntities.Player

val flowerGarden: PrimaryIndustry = PrimaryIndustry(
    "Flower Garden",
    "",
    2,
    PrimaryIndustryType.CROPS,
    listOf(4),
    PrimaryIndustryStandardEffect(1)
)

val tunaBoat: PrimaryIndustry = PrimaryIndustry(
    "Tuna Boat",
    "",
    5,
    PrimaryIndustryType.FISHING,
    listOf(12, 13, 14),
    object : PrimaryIndustryEffect {
        override fun apply(owner: Player, events: EffectEvents) {
            events.onTextOutput?.invoke("The current player rolls 2 dice.")
            val value = events.onDiceRollRequest(DiceCount.TWO)
            owner.money += value
            events.onTextOutput?.invoke("${owner.name} got ${value.asCoins} from the bank.")
        }

        override val defaultDescription: String
            get() = "On anyone's Turn: The current player rolls 2 dice. If you have a harbor, you get as many coins as the dice total."
    } requiring harbor
)

val mackerelBoat: PrimaryIndustry = PrimaryIndustry(
    "Mackerel Boat",
    "",
    2,
    PrimaryIndustryType.FISHING,
    listOf(8),
    RestaurantStandardEffect(3, "If you have a Harbor, get 3 coins from the bank on anyone's turn.") requiring harbor
)
