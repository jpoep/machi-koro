package model.cards.harbor

import model.asCoins
import model.buildings.RestaurantType
import model.buildings.SecondaryIndustryType
import model.buildings.effects.establishmentEffects.EffectEvents
import model.buildings.effects.establishmentEffects.MajorEstablishmentEffect
import model.buildings.establishments.MajorEstablishment
import model.gameEntities.Player
import kotlin.math.floor
import kotlin.math.min

val publisher = MajorEstablishment(
    "Publisher",
    "",
    5,
    listOf(7),
    object : MajorEstablishmentEffect {
        override fun apply(diceRoller: Player, others: List<Player>, events: EffectEvents) {
            others.forEach { otherPlayer ->
                val value =
                    otherPlayer.establishments.filter { it.type == RestaurantType.CAFE || it.type == SecondaryIndustryType.STORE }
                        .count()
                val moneyTaken = min(otherPlayer.money, value)
                otherPlayer.money -= moneyTaken
                diceRoller.money += moneyTaken

                events.onTextOutput?.invoke("${diceRoller.name} took ${moneyTaken.asCoins} from ${otherPlayer.name}.")
            }
        }

        override val defaultDescription: String
            get() = "Take 1 coin from each opponent for each ${RestaurantType.CAFE.displayName} and ${SecondaryIndustryType.STORE.displayName} establishment they own. (your turn only)"

    }
)

val taxOffice = MajorEstablishment(
    "Tax Office",
    "",
    4,
    listOf(8, 9),
    object : MajorEstablishmentEffect {
        override fun apply(diceRoller: Player, others: List<Player>, events: EffectEvents) {
            others
                .filter { it.money >= 10 }
                .forEach { otherPlayer ->
                    val otherPlayerOldMoney = otherPlayer.money
                    val value = floor(otherPlayerOldMoney.toDouble() / 2.0).toInt()
                    otherPlayer.money -= value
                    diceRoller.money += value

                    events.onTextOutput?.invoke("${diceRoller.name} took ${value.asCoins} from ${otherPlayer.name}, who previously had ${otherPlayerOldMoney.asCoins}.")
                }
        }

        override val defaultDescription: String
            get() = "From each opponent, who has 10+ coins, take half of their coins, rounded down. (your turn only)"

    }
)