package model.cards.harbor

import model.buildings.Landmark
import model.buildings.effects.buffs.*


val cityHall = Landmark(
    name = "City Hall",
    imageUrl = "",
    cost = 0,
    buff = FreeCoinBuff()
)

val harbor = Landmark(
    name = "Harbor",
    imageUrl = "",
    cost = 2,
    buff = AddTwoToRollBuff()
)

val airport = Landmark(
    name = "Airport",
    imageUrl = "",
    cost = 30,
    buff = FreeCoinsForPassingBuff()
)
