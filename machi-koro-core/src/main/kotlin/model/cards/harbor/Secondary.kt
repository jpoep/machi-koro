package model.cards.harbor

import model.buildings.RestaurantType
import model.buildings.SecondaryIndustryType
import model.buildings.effects.establishmentEffects.SecondaryIndustryStandardBoostedByTypeEffect
import model.buildings.effects.establishmentEffects.SecondaryIndustryStandardBoostedEffect
import model.buildings.establishments.SecondaryIndustry

val flowerShop: SecondaryIndustry = SecondaryIndustry(
    "Flower Shop",
    "",
    1,
    SecondaryIndustryType.STORE,
    listOf(6),
    object : SecondaryIndustryStandardBoostedEffect(1, SecondaryIndustryType.STORE, { it == flowerGarden }) {
        override val defaultDescription: String
            get() = "Get 1 coin from the bank for each Flower Garden you own. (your turn only)."
    }
)

val foodWarehouse: SecondaryIndustry = SecondaryIndustry(
    "Food Warehouse",
    "",
    2,
    SecondaryIndustryType.STORE,
    listOf(12, 13),
    SecondaryIndustryStandardBoostedByTypeEffect(2, SecondaryIndustryType.STORE, RestaurantType.CAFE)
)
