package model.cards.millionaire

import model.buildings.effects.establishmentEffects.RestaurantStandardEffect
import model.buildings.effects.establishmentEffects.moreThanOneConstructedLandmark
import model.buildings.effects.establishmentEffects.moreThanTwoConstructedLandmarks
import model.buildings.effects.establishmentEffects.requiring
import model.buildings.establishments.Restaurant

val frenchRestaurant = Restaurant(
    "French Restaurant",
    "",
    3,
    listOf(5),
    RestaurantStandardEffect(
        5,
        "If the player who rolled this number has 2 or more constructed Landmarks, get 5 coins from the player who rolled the dice."
    ) requiring moreThanOneConstructedLandmark
)

val membersOnlyClub = Restaurant(
    "Member's Only Club",
    "",
    4,
    listOf(12, 13, 14),
    RestaurantStandardEffect(
        Integer.MAX_VALUE - 1, // so it doesn't overflow when enhanced by the Shopping Mall
        "If the player who rolled this number has 3 or more constructed landmarks, get all of their coins."
    ) requiring moreThanTwoConstructedLandmarks
)