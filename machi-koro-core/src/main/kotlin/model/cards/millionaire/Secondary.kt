package model.cards.millionaire

import model.Card
import model.asCoins
import model.buildings.MajorEstablishmentType
import model.buildings.RestaurantType
import model.buildings.SecondaryIndustryType
import model.buildings.effects.establishmentEffects.*
import model.buildings.establishments.Establishment
import model.buildings.establishments.MajorEstablishment
import model.buildings.establishments.SecondaryIndustry
import model.gameEntities.Player
import java.lang.Integer.min

val generalStore: SecondaryIndustry = SecondaryIndustry(
    "General Store",
    "",
    0,
    SecondaryIndustryType.STORE,
    listOf(2),
    SecondaryIndustryStandardEffect(
        2,
        SecondaryIndustryType.STORE,
        "If you have less than 2 constructed landmarks, get 2 coins from the bank, on your turn only."
    )
            requiring lessThanTwoConstructedLandmarks
)

val movingCompany: SecondaryIndustry = SecondaryIndustry(
    "Moving Company",
    "",
    2,
    SecondaryIndustryType.BUSINESS,
    listOf(9, 10),
    object : Effect {
        override val defaultDescription: String
            get() = "You must give a non-${MajorEstablishmentType.MAJOR_ESTABLISHMENT.displayName} building that you own to another player. When you do, get 4 coins from the bank, on your turn only.\n"
        override val priority: Int
            get() = 2

        override fun apply(
            owner: Player,
            diceRoller: Player,
            others: List<Player>,
            triggeringCard: Card<Establishment>,
            events: EffectEvents
        ) {
            events.onTextOutput?.invoke("${diceRoller.name}'s Moving Company triggered. They must give a non ${MajorEstablishmentType.MAJOR_ESTABLISHMENT.displayName} establishment they own to a player of their choosing.")
            events.onTextOutput?.invoke("${diceRoller.name}'s buildings: ")
            diceRoller.establishments.filterNot { it is MajorEstablishment }.groupingBy { it }
                .eachCount().entries.forEach { (establishment, count) ->
                    events.onTextOutput?.invoke("\t\t${count}x ${establishment.name}")
                }

            val ownCard = diceRoller.chooseCard(
                diceRoller.establishmentCards.filterNot { it.building is MajorEstablishment },
                """Choose one of your buildings to give away, ${diceRoller.name}."""
            )

            events.onTextOutput?.invoke("The other players' buildings: ")
            others.forEach { player ->
                events.onTextOutput?.invoke("\t${player.name}:")
                player.establishments.filterNot { it is MajorEstablishment }.groupingBy { it }
                    .eachCount().entries.forEach { (establishment, count) ->
                        events.onTextOutput?.invoke("\t\t${count}x ${establishment.name}")
                    }
            }

            events.onTextOutput?.invoke("Choose another player to give your ${ownCard.building.name} to.")
            val target = diceRoller.choosePlayer(others)

            diceRoller.cards.remove(ownCard)
            target.cards.add(ownCard)
            diceRoller.money += 4

            events.onTextOutput?.invoke("${diceRoller.name} passed their ${ownCard.building.name} to ${target.name} and received ${4.asCoins} for it.")
        }
    })

val loanOffice: SecondaryIndustry = SecondaryIndustry(
    "Loan Office",
    "",
    -5,
    SecondaryIndustryType.BUSINESS,
    listOf(5, 6),
    object : SecondaryIndustryEffect {
        override fun apply(diceRoller: Player, events: EffectEvents) {
            val moneyPaid = min(diceRoller.money, 2)
            diceRoller.money -= moneyPaid
            events.onTextOutput?.invoke("${diceRoller.name} paid ${moneyPaid.asCoins} to the bank.")
        }

        override val defaultDescription: String
            get() = "When you construct this building, get 5 coins from the bank. When this building is activated, pay 2 coins to the bank, on your turn only."
    }
)

val winery: SecondaryIndustry = SecondaryIndustry(
    "Winery",
    "",
    3,
    SecondaryIndustryType.FACTORY,
    listOf(9),
    object : SecondaryIndustryStandardBoostedEffect(6, SecondaryIndustryType.FACTORY, { it == vineyard }) {
        override val defaultDescription: String
            get() = "Get 6 coins for each vineyard you own, on your turn only. Then, close this building for renovation."
    }.closesForRenovation()
)


val demolitionCompany: SecondaryIndustry = SecondaryIndustry(
    "Demolition Company",
    "",
    2,
    SecondaryIndustryType.BUSINESS,
    listOf(4),
    object : Effect {
        override val defaultDescription: String
            get() = "If possible, you must demolish one of your constructed landmarks by turning it back over to its unconstructed side. When you do, get 8 coins from the bank, on your turn only."
        override val priority: Int
            get() = 2

        override fun apply(
            owner: Player,
            diceRoller: Player,
            others: List<Player>,
            triggeringCard: Card<Establishment>,
            events: EffectEvents
        ) {
            events.onTextOutput?.invoke("${diceRoller.name}'s Demolition Company triggered. They must destroy an already constructed Landmark and will receive 8 coins for doing so.")

            if (diceRoller.constructedLandmarks.isEmpty()) {
                events.onTextOutput?.invoke("However, they don't have any constructed landmarks.")
                return;
            }

            val landmark = diceRoller.chooseCard(
                diceRoller.constructedLandmarkCards,
                "Choose one of your landmarks to demolish, ${diceRoller.name}."
            )

            diceRoller.cards.remove(landmark)
            diceRoller.money += 8

            events.onTextOutput?.invoke("${diceRoller.name} demolished their ${landmark.building.name} and received 8 coins for doing so.")
        }
    }
)

val sodaBottlingPlant: SecondaryIndustry = SecondaryIndustry(
    "Soda Bottling Plant",
    "",
    5,
    SecondaryIndustryType.FACTORY,
    listOf(11),
    object : Effect {
        override val defaultDescription: String
            get() = "Get 1 coin from the bank for every ${RestaurantType.CAFE} establishment owned by all players, on your turn only."
        override val priority: Int
            get() = 2

        override fun apply(
            owner: Player,
            diceRoller: Player,
            others: List<Player>,
            triggeringCard: Card<Establishment>,
            events: EffectEvents
        ) {
            val money = (others + diceRoller)
                .flatMap { it.establishments }
                .filter { it.type == RestaurantType.CAFE }
                .count()

            diceRoller.money += money

            events.onTextOutput?.invoke("${diceRoller.name} received ${money.asCoins} from the bank.")
        }
    }
)
