package model.cards.millionaire

import model.Card
import model.asCoins
import model.buildings.MajorEstablishmentType
import model.buildings.effects.buffs.InvestmentBuff
import model.buildings.effects.establishmentEffects.Effect
import model.buildings.effects.establishmentEffects.EffectEvents
import model.buildings.effects.establishmentEffects.MajorEstablishmentEffect
import model.buildings.establishments.Establishment
import model.buildings.establishments.MajorEstablishment
import model.gameEntities.Player
import kotlin.math.min

val park = MajorEstablishment(
    "Park",
    "",
    3,
    listOf(11, 12, 13),
    object : MajorEstablishmentEffect {
        override fun apply(diceRoller: Player, others: List<Player>, events: EffectEvents) {
            events.onTextOutput?.invoke("${diceRoller.name}'s Park triggered. The coins of all players will be distributed evenly, with the bank making up the difference.")
            val allPlayers = (others + diceRoller)
            val totalMoney = allPlayers.map { it.money }.sum()
            val playerCount = allPlayers.count()
            val addedDifference = playerCount - (totalMoney % playerCount)

            val share = (totalMoney + addedDifference) / playerCount

            allPlayers.forEach {
                it.money = share
            }
            events.onTextOutput?.invoke("Each player now has ${share.asCoins}. The bank paid an additional ${addedDifference.asCoins} to make it even.")
        }

        override val defaultDescription: String
            get() = "Redistribute all players' coins evenly among all players, on your turn only. If there is an uneven amount of coins, take coins from the bank to make up the difference."

    }
)

val renovationCompany = MajorEstablishment(
    "Renovation Company",
    "",
    4,
    listOf(8),
    object : MajorEstablishmentEffect {
        override fun apply(diceRoller: Player, others: List<Player>, events: EffectEvents) {
            events.onTextOutput?.invoke("${diceRoller.name}'s Renovation Company triggered. They must choose a building that will be closed for renovation for all other players. ${diceRoller.name} gets 1 coin from each player for each of their buildings closed for renovation.")
            events.onTextOutput?.invoke("Here is a list of all players' buildings: ")

            others.forEach { player ->
                events.onTextOutput?.invoke("\t${player.name}:")
                player.establishments.filterNot { it is MajorEstablishment }.groupingBy { it }
                    .eachCount().entries.forEach { (establishment, count) ->
                        events.onTextOutput?.invoke("\t\t${count}x ${establishment.name}")
                    }
            }

            val building =
                diceRoller.chooseBuilding(
                    others
                        .flatMap { it.establishments }
                        .filterNot { it is MajorEstablishment }
                        .distinct(),
                    "Choose a building type to be closed, ${diceRoller.name}."
                )

            others.forEach { player ->
                val moneyOwed = player.establishmentCards
                    .filter { it.building == building }
                    .onEach { it.closedForRenovation = true }
                    .count()

                val moneyPaid = min(moneyOwed, player.money)
                diceRoller.money += moneyPaid
                player.money -= moneyPaid

                events.onTextOutput?.invoke("${diceRoller.name} got ${moneyPaid.asCoins} from ${player.name}.")
            }
        }

        override val defaultDescription: String
            get() = "Choose a non-${MajorEstablishmentType.MAJOR_ESTABLISHMENT} establishment. All buildings owned by any player of that type are closed for renovations. Get 1 coin from each player for each of their buildings closed for renovation, on your turn only."
    }
)


val techStartup = MajorEstablishment(
    "Tech Startup",
    "",
    1,
    listOf(10),
    object : Effect {
        override val defaultDescription: String
            get() = "At the end of each of your turns, you may place 1 coin on this card. The total placed here is your investment. When activated, get an amount equal to your investment from all players, on your turn only."
        override val priority: Int
            get() = 3

        override fun apply(
            owner: Player,
            diceRoller: Player,
            others: List<Player>,
            triggeringCard: Card<Establishment>,
            events: EffectEvents
        ) {
            val moneyOwed = triggeringCard.moneyInvested
            others.forEach {
                val moneyPaid = min(it.money, moneyOwed)
                it.money -= moneyPaid
                diceRoller.money += moneyPaid
                events.onTextOutput?.invoke("${diceRoller.name} got ${moneyPaid.asCoins} from ${it.name}.")
            }
        }
    },
    InvestmentBuff()
)

val conventionCenter = MajorEstablishment(
    "Convention Center",
    "",
    7,
    listOf(10),
    object : MajorEstablishmentEffect {
        override fun apply(diceRoller: Player, others: List<Player>, events: EffectEvents) {
            events.onTextOutput?.invoke(defaultDescription)
            events.onTextOutput?.invoke("${diceRoller.name}'s buildings:")
            val buildings = diceRoller.establishments.filterNot { it is MajorEstablishment }
            buildings.groupingBy { it }.eachCount().forEach { (establishment, count) ->
                events.onTextOutput?.invoke("\t${count}x ${establishment.name} -- ${establishment.effect.description}")
            }
            val building = diceRoller.chooseBuilding(buildings, "Type the name of the building you want to activate, ${diceRoller.name}.") as Establishment
            events.onConventionCenterEffect(building)
        }

        override val defaultDescription: String
            get() = "You may choose to activate another of your non ${MajorEstablishmentType.MAJOR_ESTABLISHMENT} establishments in place of this one, on your turn only. If you do, return this card to the market. All establishments of that type will activate when using this card."
    }
)