package model.cards.millionaire
import model.buildings.PrimaryIndustryType
import model.buildings.effects.establishmentEffects.PrimaryIndustryStandardEffect
import model.buildings.effects.establishmentEffects.lessThanTwoConstructedLandmarks
import model.buildings.effects.establishmentEffects.requiring
import model.buildings.establishments.PrimaryIndustry

val cornField: PrimaryIndustry = PrimaryIndustry(
    "Corn Field",
    "",
    2,
    PrimaryIndustryType.CROPS,
    listOf(3, 4),
    PrimaryIndustryStandardEffect(
        1, "If you have less than 2 landmarks built, get 1 coin from the bank, on anyone's turn."
    ) requiring lessThanTwoConstructedLandmarks
)

val vineyard: PrimaryIndustry = PrimaryIndustry(
    "Vineyard",
    "",
    3,
    PrimaryIndustryType.CROPS,
    listOf(7),
    PrimaryIndustryStandardEffect(3)
)