package model.cards.base

import model.buildings.Landmark
import model.buildings.effects.buffs.AdditionalRollBuff
import model.buildings.effects.buffs.CafeAndStoreBuff
import model.buildings.effects.buffs.DoubleDiceBuff
import model.buildings.effects.buffs.ReRollBuff

val trainStation = Landmark(
    name = "Train Station",
    imageUrl = "",
    cost = 4,
    buff = DoubleDiceBuff()
)

val shoppingMall = Landmark(
    name = "Shopping Mall",
    imageUrl = "",
    cost = 10,
    buff = CafeAndStoreBuff()
)

val amusementPark = Landmark(
    name = "Amusement Park",
    imageUrl = "",
    cost = 16,
    buff = AdditionalRollBuff()
)

val radioTower = Landmark(
    name = "Radio Tower",
    imageUrl = "",
    cost = 22,
    buff = ReRollBuff()
)

