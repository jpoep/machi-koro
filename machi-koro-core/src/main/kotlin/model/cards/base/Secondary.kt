package model.cards.base

import model.buildings.PrimaryIndustryType
import model.buildings.SecondaryIndustryType
import model.buildings.effects.establishmentEffects.SecondaryIndustryStandardBoostedByTypeEffect
import model.buildings.effects.establishmentEffects.SecondaryIndustryStandardEffect
import model.buildings.establishments.SecondaryIndustry

val bakery: SecondaryIndustry = SecondaryIndustry(
    "Bakery",
    "",
    1,
    SecondaryIndustryType.STORE,
    listOf(2, 3),
    SecondaryIndustryStandardEffect(1, SecondaryIndustryType.STORE)
)

val convenienceStore: SecondaryIndustry = SecondaryIndustry(
    "Convenience Store",
    "",
    2,
    SecondaryIndustryType.STORE,
    listOf(4),
    SecondaryIndustryStandardEffect(2, SecondaryIndustryType.STORE)
)

val cheeseFactory: SecondaryIndustry = SecondaryIndustry(
    "Cheese Factory",
    "",
    2,
    SecondaryIndustryType.FACTORY,
    listOf(7),
    SecondaryIndustryStandardBoostedByTypeEffect(
        3,
        SecondaryIndustryType.FACTORY,
        PrimaryIndustryType.LIVESTOCK
    )
)

val furnitureFactory: SecondaryIndustry = SecondaryIndustry(
    "Furniture Factory",
    "",
    3,
    SecondaryIndustryType.FACTORY,
    listOf(8),
    SecondaryIndustryStandardBoostedByTypeEffect(
        3,
        SecondaryIndustryType.FACTORY,
        PrimaryIndustryType.INDUSTRY
    )
)

val fruitAndVegetableMarket: SecondaryIndustry = SecondaryIndustry(
    "Fruit and Vegetable Market",
    "",
    2,
    SecondaryIndustryType.FRUIT_MARKET,
    listOf(11, 12),
    SecondaryIndustryStandardBoostedByTypeEffect(
        2,
        SecondaryIndustryType.FRUIT_MARKET,
        PrimaryIndustryType.CROPS
    )
)