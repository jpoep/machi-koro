package model.cards.base

import model.buildings.PrimaryIndustryType
import model.buildings.effects.establishmentEffects.PrimaryIndustryStandardEffect
import model.buildings.establishments.PrimaryIndustry

val wheatField: PrimaryIndustry = PrimaryIndustry(
    "Wheat Field",
    "",
    1,
    PrimaryIndustryType.CROPS,
    listOf(1),
    PrimaryIndustryStandardEffect(1)
)

val ranch: PrimaryIndustry = PrimaryIndustry(
    "Ranch",
    "",
    1,
    PrimaryIndustryType.LIVESTOCK,
    listOf(2),
    PrimaryIndustryStandardEffect(1)
)

val forest: PrimaryIndustry = PrimaryIndustry(
    "Forest",
    "",
    3,
    PrimaryIndustryType.INDUSTRY,
    listOf(5),
    PrimaryIndustryStandardEffect(1)
)

val mine: PrimaryIndustry = PrimaryIndustry(
    "Mine",
    "",
    6,
    PrimaryIndustryType.INDUSTRY,
    listOf(9),
    PrimaryIndustryStandardEffect(5)
)

val appleOrchard: PrimaryIndustry = PrimaryIndustry(
    "Apple Orchard",
    "",
    3,
    PrimaryIndustryType.CROPS,
    listOf(10),
    PrimaryIndustryStandardEffect(3)
)