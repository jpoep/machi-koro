package model.cards.base

import model.buildings.effects.establishmentEffects.RestaurantStandardEffect
import model.buildings.establishments.Restaurant

val cafe: Restaurant = Restaurant(
    "Cafe",
    "",
    2,
    listOf(3),
    RestaurantStandardEffect(1)
)

val familyRestaurant: Restaurant = Restaurant(
    "Family Restaurant",
    "",
    3,
    listOf(9, 10),
    RestaurantStandardEffect(3)
)