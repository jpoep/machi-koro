package model.cards.base

import model.asCoins
import model.buildings.MajorEstablishmentType
import model.buildings.effects.establishmentEffects.EffectEvents
import model.buildings.effects.establishmentEffects.MajorEstablishmentEffect
import model.buildings.establishments.MajorEstablishment
import model.gameEntities.Player
import kotlin.math.min

val stadium: MajorEstablishment = MajorEstablishment(
    "Stadium",
    "",
    6,
    listOf(6),
    object : MajorEstablishmentEffect {
        private val value = 2

        override val defaultDescription: String
            get() = "Get ${value.asCoins} from all console.getPlayers, on your turn only."

        override fun apply(diceRoller: Player, others: List<Player>, events: EffectEvents) {
            others.forEach {
                val moneyTaken = min(it.money, value)
                it.money -= moneyTaken
                diceRoller.money += moneyTaken

                events.onTextOutput?.invoke("${diceRoller.name} took ${moneyTaken.asCoins} from ${it.name}.")
            }
        }

    }
)

val tvStation: MajorEstablishment = MajorEstablishment(
    "TV Station",
    "",
    7,
    listOf(6),
    object : MajorEstablishmentEffect {
        private val value = 5

        override val defaultDescription: String
            get() = "Take ${value.asCoins} from any one player, on your turn only."

        override fun apply(diceRoller: Player, others: List<Player>, events: EffectEvents) {
            events.onTextOutput?.invoke("${diceRoller.name}'s TV Station triggered. They may take ${value.asCoins} from a player of their choosing.")
            val target = diceRoller.choosePlayer(others)
            val moneyTaken = min(target.money, value)
            target.money -= moneyTaken
            diceRoller.money += moneyTaken

            events.onTextOutput?.invoke("${diceRoller.name} took ${moneyTaken.asCoins} from ${target.name}.")
        }
    }
)


val businessCenter: MajorEstablishment = MajorEstablishment(
    "Business Center",
    "",
    8,
    listOf(6),
    object : MajorEstablishmentEffect {
        override val defaultDescription: String
            get() = "Trade One Non ${MajorEstablishmentType.MAJOR_ESTABLISHMENT.displayName} establishment with another player, on your turn only."

        override fun apply(diceRoller: Player, others: List<Player>, events: EffectEvents) {
            events.onTextOutput?.invoke("${diceRoller.name}'s Business Center triggered. They may trade a non ${MajorEstablishmentType.MAJOR_ESTABLISHMENT.displayName} establishment with a player of their choosing.")
            events.onTextOutput?.invoke("Other console.getPlayers' buildings: ")
            others.forEach { player ->
                events.onTextOutput?.invoke("\t${player.name}:")
                player.establishments.filterNot { it is MajorEstablishment }.groupingBy { it }.eachCount().entries.forEach {(establishment, count) ->
                    events.onTextOutput?.invoke("\t\t${count}x ${establishment.name}")
                }
            }
            val target = diceRoller.choosePlayer(others)
            val ownBuilding = diceRoller.chooseCard(
                diceRoller.establishmentCards.filterNot { it.building is MajorEstablishment },
                """Choose one of your buildings to trade to ${target.name}, ${diceRoller.name}."""
            )
            val targetBuilding = diceRoller.chooseCard(
                target.establishmentCards.filterNot { it.building is MajorEstablishment },
                """Choose a building to take from ${target.name}, ${diceRoller.name}."""
            )

            diceRoller.cards.remove(ownBuilding)
            diceRoller.cards.add(targetBuilding)
            target.cards.remove(targetBuilding)
            target.cards.add(ownBuilding)

            events.onTextOutput?.invoke("${diceRoller.name} traded their ${ownBuilding.building.name} for ${target.name}'s ${targetBuilding.building.name}.")
        }
    }
)
