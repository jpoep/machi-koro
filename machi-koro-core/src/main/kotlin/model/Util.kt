package model

val Int.asCoins: String
    get() = """$this coin${if (this > 1) "s" else ""}"""
