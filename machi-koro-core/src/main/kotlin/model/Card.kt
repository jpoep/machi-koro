package model

import model.buildings.Building

data class Card<out T: Building>(val building: T, var closedForRenovation: Boolean = false, var moneyInvested: Int = 0) // moneyInvested should actually only be on the tech startup but that'd be a huge pain