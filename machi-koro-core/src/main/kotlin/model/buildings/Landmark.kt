package model.buildings

import model.buildings.effects.buffs.Buff

class Landmark(
    name: String,
    imageUrl: String,
    cost: Int,
    buff: Buff
) : Building(name, imageUrl, cost, MajorEstablishmentType.MAJOR_ESTABLISHMENT, buff)