package model.buildings.effects.establishmentEffects

import model.asCoins
import model.buildings.RestaurantType
import model.buildings.effects.buffs.CafeAndStoreBuff
import model.gameEntities.Player
import kotlin.math.min

class RestaurantStandardEffect(private val value: Int, override val customDescription: String? = null) :
    RestaurantEffect {

    override val defaultDescription: String
        get() = """Get ${value.asCoins} from the player who rolled the dice."""

    override fun apply(owner: Player, diceRoller: Player, events: EffectEvents) {
        val ownerBuff: CafeAndStoreBuff? =
            owner.activeBuffs.filterIsInstance<CafeAndStoreBuff>().firstOrNull()

        val buffedValue = ownerBuff?.run { getBoostedValue(value, RestaurantType.CAFE) }
            ?: value // TODO: put logic into CafeAndStoreBuff; otherwise each effect has to check for it individually

        val availableMoney = min(diceRoller.money, buffedValue)

        owner.money += availableMoney
        diceRoller.money -= availableMoney

        events.onTextOutput?.invoke("${diceRoller.name} paid ${owner.name} ${availableMoney.asCoins} for their restaurant.${if (ownerBuff != null) " ${owner.name}'s Shopping Mall increased the price by 1." else ""}")
    }
}
