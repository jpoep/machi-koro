package model.buildings.effects.establishmentEffects

import model.asCoins
import model.gameEntities.Player

class PrimaryIndustryStandardEffect(private val value: Int, override val customDescription: String? = null) :
    PrimaryIndustryEffect {

    override val defaultDescription: String
        get() = """Get ${value.asCoins} from the bank on anyone's turn."""

    override fun apply(owner: Player, events: EffectEvents) {
        owner.money += value
        events.onTextOutput?.invoke("${owner.name} got ${value.asCoins} from the bank.")
    }
}
