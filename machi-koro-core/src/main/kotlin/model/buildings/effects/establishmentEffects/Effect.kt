package model.buildings.effects.establishmentEffects

import model.buildings.Building
import model.buildings.establishments.Establishment
import model.Card
import model.gameEntities.DiceCount
import model.gameEntities.Player

data class EffectEvents(
    var onDiceRollRequest: ((DiceCount) -> Int),
    var onConventionCenterEffect: ((establishment: Establishment) -> Unit),
    var onTextOutput: ((String) -> Unit)? = null
)

interface Effect {
    val customDescription: String?
        get() = null

    val defaultDescription: String

    val description: String
        get() = customDescription ?: defaultDescription

    val priority: Int

    fun apply(
        owner: Player,
        diceRoller: Player,
        others: List<Player>,
        triggeringCard: Card<Establishment>,
        events: EffectEvents
    )
}

typealias EffectCondition = (owner: Player, diceRoller: Player, others: List<Player>, events: EffectEvents) -> Boolean

class ConditionalEffectDecorator(private val effect: Effect, private val predicate: EffectCondition) :
    Effect by effect {
    override fun apply(
        owner: Player,
        diceRoller: Player,
        others: List<Player>,
        triggeringCard: Card<Establishment>,
        events: EffectEvents
    ) {
        if (predicate(owner, diceRoller, others, events)) {
            effect.apply(owner, diceRoller, others, triggeringCard, events)
        }
    }
}

class RequiredBuildingEffectDecorator(private val effect: Effect, private val requiredBuilding: Building) :
    Effect by effect {

    override fun apply(
        owner: Player,
        diceRoller: Player,
        others: List<Player>,
        triggeringCard: Card<Establishment>,
        events: EffectEvents
    ) {
        if (owner.buildings.contains(requiredBuilding)) {
            effect.apply(owner, diceRoller, others, triggeringCard, events)
        } else {
            events.onTextOutput?.invoke("Except it didn't because ${owner.name} doesn't have a ${requiredBuilding.name}.")
        }
    }
}

class RenovationAwareEffectDecorator(private val effect: Effect) : Effect by effect {
    override fun apply(
        owner: Player,
        diceRoller: Player,
        others: List<Player>,
        triggeringCard: Card<Establishment>,
        events: EffectEvents
    ) {
        if (triggeringCard.closedForRenovation) {
            events.onTextOutput?.invoke("${triggeringCard.building.name} is currently closed for renovation and will not produce. Renovating will be finished in the next production round.")
            triggeringCard.closedForRenovation = false
        } else {
            effect.apply(owner, diceRoller, others, triggeringCard, events)
        }
    }
}

class RenovationEffectDecorator(private val effect: Effect) : Effect by effect {
    override fun apply(
        owner: Player,
        diceRoller: Player,
        others: List<Player>,
        triggeringCard: Card<Establishment>,
        events: EffectEvents
    ) {
        effect.apply(owner, diceRoller, others, triggeringCard, events)
        events.onTextOutput?.invoke("${triggeringCard.building.name} will be closed for renovation in the next round.")
        triggeringCard.closedForRenovation = true
    }
}

infix fun Effect.requiring(predicate: EffectCondition): Effect = ConditionalEffectDecorator(this, predicate)

infix fun Effect.requiring(building: Building): Effect = RequiredBuildingEffectDecorator(this, building)

fun Effect.closesForRenovation(): Effect = RenovationEffectDecorator(this)

fun Effect.renovationAware(): Effect = RenovationAwareEffectDecorator(this)

val lessThanTwoConstructedLandmarks: EffectCondition =
    { owner, _, _, events ->
        (owner.constructedLandmarks.size < 2).also {
            if (!it) events.onTextOutput?.invoke("${owner.name} has constructed more than 1 landmark, rendering this establishment useless.")
        }
    }

val moreThanOneConstructedLandmark: EffectCondition =
    { _, diceRoller, _, events ->
        (diceRoller.constructedLandmarks.size > 1).also {
            if (!it) events.onTextOutput?.invoke("${diceRoller.name} has constructed less than 2 landmarks, rendering this establishment useless.")
        }
    }

val moreThanTwoConstructedLandmarks: EffectCondition =
    { _, diceRoller, _, events ->
        (diceRoller.constructedLandmarks.size > 2).also {
            if (!it) events.onTextOutput?.invoke("${diceRoller.name} has constructed less than 3 landmarks, rendering this establishment useless.")
        }
    }


interface MajorEstablishmentEffect : Effect {
    override val priority: Int
        get() = 3

    override fun apply(
        owner: Player,
        diceRoller: Player,
        others: List<Player>,
        triggeringCard: Card<Establishment>,
        events: EffectEvents
    ) {
        apply(owner, others, events)
    }

    fun apply(diceRoller: Player, others: List<Player>, events: EffectEvents)
}

interface PrimaryIndustryEffect : Effect {
    override val priority: Int
        get() = 2

    override fun apply(
        owner: Player,
        diceRoller: Player,
        others: List<Player>,
        triggeringCard: Card<Establishment>,
        events: EffectEvents
    ) {
        apply(owner, events)
    }

    fun apply(owner: Player, events: EffectEvents)
}


interface RestaurantEffect : Effect {
    override val priority: Int
        get() = 1

    override fun apply(
        owner: Player,
        diceRoller: Player,
        others: List<Player>,
        triggeringCard: Card<Establishment>,
        events: EffectEvents
    ) {
        apply(owner, diceRoller, events)
    }

    fun apply(owner: Player, diceRoller: Player, events: EffectEvents)

}

interface SecondaryIndustryEffect : Effect {
    override val priority: Int
        get() = 2

    override fun apply(
        owner: Player,
        diceRoller: Player,
        others: List<Player>,
        triggeringCard: Card<Establishment>,
        events: EffectEvents
    ) {
        apply(owner, events)
    }

    fun apply(diceRoller: Player, events: EffectEvents)

}
