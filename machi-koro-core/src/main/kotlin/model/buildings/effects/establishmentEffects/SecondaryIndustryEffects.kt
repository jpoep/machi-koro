package model.buildings.effects.establishmentEffects

import model.asCoins
import model.buildings.Building
import model.buildings.BuildingType
import model.buildings.SecondaryIndustryType
import model.buildings.effects.buffs.CafeAndStoreBuff
import model.gameEntities.Player

open class SecondaryIndustryStandardEffect(
    private val value: Int,
    private val buildingType: SecondaryIndustryType,
    override val customDescription: String? = null
    ) : SecondaryIndustryEffect {

    override fun apply(diceRoller: Player, events: EffectEvents) {
        val buffedValue =
            diceRoller.activeBuffs.filterIsInstance<CafeAndStoreBuff>().firstOrNull()
                ?.run { getBoostedValue(value, buildingType) }
                ?: value // TODO: put logic into CafeAndStoreBuff; otherwise each effect has to check for it individually

        diceRoller.money += buffedValue

        events.onTextOutput?.invoke("${diceRoller.name} got ${buffedValue.asCoins} from the bank.")
    }

    override val defaultDescription: String
        get() = "Get ${value.asCoins}  from the bank on your turn."

}

abstract class SecondaryIndustryStandardBoostedEffect(
    private val value: Int,
    private val buildingType: SecondaryIndustryType,
    private val boostingBuildings: (building: Building) -> Boolean
) : SecondaryIndustryEffect {

    override fun apply(diceRoller: Player, events: EffectEvents) {
        val buffedValue =
            diceRoller.activeBuffs.filterIsInstance<CafeAndStoreBuff>().firstOrNull()
                ?.run { getBoostedValue(value, buildingType) }
                ?: value // TODO: put logic into CafeAndStoreBuff; otherwise each effect has to check for it individually

        val boostingBuildingsCount = diceRoller.establishments.filter(boostingBuildings).count()

        diceRoller.money += buffedValue * boostingBuildingsCount

        events.onTextOutput?.invoke("${diceRoller.name} got ${(buffedValue * boostingBuildingsCount).asCoins} from the bank (${buffedValue}x${boostingBuildingsCount}).")
    }
}

open class SecondaryIndustryStandardBoostedByTypeEffect(
    private val value: Int,
    buildingType: SecondaryIndustryType,
    private val boostingType: BuildingType,
    override val customDescription: String? = null
    ) : SecondaryIndustryStandardBoostedEffect(value, buildingType, { it.type == boostingType }) {

    override val defaultDescription: String
        get() = "Get ${value.asCoins}  from the bank for each ${boostingType.displayName} establishment that you own, on your turn only."

}
