package model.buildings.effects.buffs

class FreeCoinBuff : Buff {
    override val description: String
        get() = "Immediately before buying establishments, if you have 0 coins, get 1 from the bank."
}