package model.buildings.effects.buffs

class AdditionalRollBuff : Buff {
    override val description: String
        get() = "If your roll doubles, you get an additional turn for free."
}