package model.buildings.effects.buffs

class ReRollBuff : Buff {
    override val description: String
        get() = "Once every turn, you can choose to re-roll your dice."
}