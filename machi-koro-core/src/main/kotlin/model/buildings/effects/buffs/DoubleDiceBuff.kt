package model.buildings.effects.buffs

class DoubleDiceBuff : Buff {
    override val description: String
        get() = "You may roll one or two dice."
}