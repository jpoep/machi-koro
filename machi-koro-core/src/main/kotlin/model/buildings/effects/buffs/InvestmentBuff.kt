package model.buildings.effects.buffs

class InvestmentBuff : Buff {
    override val description: String
        get() = "At the end of your turn, you may choose to invest 1 coin into your Tech Startup."
}