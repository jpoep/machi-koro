package model.buildings.effects.buffs

import model.buildings.BuildingType
import model.buildings.RestaurantType
import model.buildings.SecondaryIndustryType

class CafeAndStoreBuff : Buff {
    override val description: String
        get() = "Each of your ${RestaurantType.CAFE.displayName} and ${SecondaryIndustryType.STORE.displayName} establishments generate one additional coin."

    fun getBoostedValue(value: Int, buildingType: BuildingType): Int =
        when (buildingType) {
            RestaurantType.CAFE -> value + 1
            SecondaryIndustryType.STORE -> value + 1
            else -> value
        }

}