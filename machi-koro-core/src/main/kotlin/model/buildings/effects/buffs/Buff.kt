package model.buildings.effects.buffs

interface Buff {
    val description: String
}
