package model.buildings.effects.buffs

class FreeCoinsForPassingBuff : Buff {
    override val description: String
        get() = "If you build nothing on your turn, you get $amount from the bank."

    val amount: Int = 10
}
