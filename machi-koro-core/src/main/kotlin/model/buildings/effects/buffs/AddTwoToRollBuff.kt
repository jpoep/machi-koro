package model.buildings.effects.buffs

class AddTwoToRollBuff : Buff {
    override val description: String
        get() = "If the dice total is 10 or more, you may add 2 to the total, on your turn only."
}
