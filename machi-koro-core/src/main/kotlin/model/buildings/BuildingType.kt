package model.buildings

interface BuildingType {
    val displayName: String
    val iconUrl: String?
}

enum class PrimaryIndustryType(override val displayName: String, override val iconUrl: String) :
    BuildingType {
    CROPS("Crops", ""),
    LIVESTOCK("Livestock", ""),
    INDUSTRY("Industry", ""),
    FISHING("Fishing", ""),
}

enum class SecondaryIndustryType(override val displayName: String, override val iconUrl: String) :
    BuildingType {
    STORE("Store", ""),
    FACTORY("Factory", ""),
    FRUIT_MARKET("Fruit Market", ""),
    BUSINESS("Business", ""),
}

enum class RestaurantType(override val displayName: String, override val iconUrl: String) :
    BuildingType {
    CAFE("Cafe", "")
}

enum class MajorEstablishmentType(override val displayName: String, override val iconUrl: String) :
    BuildingType {
    MAJOR_ESTABLISHMENT("Major Establishment", "")
}