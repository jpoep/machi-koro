package model.buildings.establishments

import model.buildings.PrimaryIndustryType
import model.buildings.effects.establishmentEffects.Effect
import model.buildings.effects.establishmentEffects.PrimaryIndustryEffect
import model.gameEntities.Player

open class PrimaryIndustry(
    name: String,
    imageUrl: String,
    cost: Int,
    type: PrimaryIndustryType,
    activationTriggers: List<Int>,
    effect: Effect
) : Establishment(name, imageUrl, cost, type, activationTriggers, effect) {
    override fun willTrigger(
        ownersTurn: Boolean,
        diceRoller: Player,
        owner: Player
    ): Boolean = true
}