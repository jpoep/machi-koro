package model.buildings.establishments

import model.buildings.MajorEstablishmentType
import model.buildings.effects.buffs.Buff
import model.buildings.effects.establishmentEffects.Effect
import model.buildings.effects.establishmentEffects.MajorEstablishmentEffect
import model.gameEntities.Player

open class MajorEstablishment(
    name: String,
    imageUrl: String,
    cost: Int,
    activationTriggers: List<Int>,
    effect: Effect,
    buff: Buff? = null
) : Establishment(name, imageUrl, cost, MajorEstablishmentType.MAJOR_ESTABLISHMENT, activationTriggers, effect, buff) {
    override fun willTrigger(
        ownersTurn: Boolean,
        diceRoller: Player,
        owner: Player
    ) = ownersTurn
}