package model.buildings.establishments

import model.buildings.SecondaryIndustryType
import model.buildings.effects.establishmentEffects.Effect
import model.buildings.effects.establishmentEffects.SecondaryIndustryEffect
import model.gameEntities.Player

class SecondaryIndustry(
    name: String,
    imageUrl: String,
    cost: Int,
    type: SecondaryIndustryType,
    activationTriggers: List<Int>,
    effect: Effect
) : Establishment(name, imageUrl, cost, type, activationTriggers, effect) {
    override fun willTrigger(
        ownersTurn: Boolean,
        diceRoller: Player,
        owner: Player
    ): Boolean = ownersTurn
}