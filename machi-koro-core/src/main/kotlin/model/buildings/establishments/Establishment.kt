package model.buildings.establishments

import model.Card
import model.buildings.Building
import model.buildings.BuildingType
import model.buildings.effects.buffs.Buff
import model.buildings.effects.establishmentEffects.*
import model.gameEntities.Player

@Suppress("LeakingThis")
abstract class Establishment(
    name: String,
    imageUrl: String,
    cost: Int,
    type: BuildingType,
    val activationTriggers: List<Int>,
    initialEffect: Effect,
    buff: Buff? = null
) : Building(name, imageUrl, cost, type, buff) {

    abstract fun willTrigger(
        ownersTurn: Boolean,
        diceRoller: Player,
        owner: Player
    ): Boolean

    val effect = initialEffect.renovationAware()

}