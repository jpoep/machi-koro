package model.buildings.establishments

import model.buildings.RestaurantType
import model.buildings.effects.establishmentEffects.Effect
import model.buildings.effects.establishmentEffects.RestaurantEffect
import model.gameEntities.Player

open class Restaurant(
    name: String,
    imageUrl: String,
    cost: Int,
    activationTriggers: List<Int>,
    effect: Effect
) : Establishment(name, imageUrl, cost, RestaurantType.CAFE, activationTriggers, effect) {
    override fun willTrigger(
        ownersTurn: Boolean,
        diceRoller: Player,
        owner: Player
    ): Boolean = !ownersTurn
}