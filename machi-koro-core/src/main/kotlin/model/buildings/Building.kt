package model.buildings

import model.buildings.effects.buffs.Buff

abstract class Building(val name: String,
                    val imageUrl: String?,
                    val cost: Int,
                    val type: BuildingType,
                    val buff: Buff? = null)