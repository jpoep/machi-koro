package model.gameEntities

import kotlin.random.Random
import kotlin.random.nextInt

fun rollWithOne(): Int = Random.nextInt(1..6)
fun rollWithTwo(): Pair<Int, Int> =
    Random.nextInt(1..6) to Random.nextInt(1..6)

fun rollWith1337(): Int = (1..1337).map { Random.nextInt(1..6) }.sum()

enum class DiceCount { ONE, TWO, LEET }