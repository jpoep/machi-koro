package model.gameEntities

import model.Card
import model.buildings.Building
import model.buildings.Landmark
import model.buildings.establishments.Establishment
import model.buildings.establishments.MajorEstablishment
import model.cards.base.*
import model.cards.harbor.*
import model.cards.millionaire.*
import java.util.*


class CardPool(
    cheapPoolSize: Int = 6,
    expensivePoolSize: Int = 6,
    majorPoolSize: Int = 2,
    cheapStack: Stack<Card<Establishment>>,
    expensiveStack: Stack<Card<Establishment>>,
    majorStack: Stack<Card<Establishment>>,
    availableLandmarks: Set<Landmark>
) {

    private val cheapCardPool = SingleExhaustibleCardPool(cheapPoolSize, cheapStack)
    private val expensiveCardPool = SingleExhaustibleCardPool(expensivePoolSize, expensiveStack)
    private val majorCardPool = SingleExhaustibleCardPool(majorPoolSize, majorStack)
    private val landmarkCardPool = SingleInexhaustibleCardPool(availableLandmarks)

    private val cardPools = listOf(cheapCardPool, expensiveCardPool, majorCardPool, landmarkCardPool)

    fun drawCard(card: Card<Building>): Card<Building> {
        return cardPools
            .mapNotNull { it.drawCard(card) }.firstOrNull()
            ?: throw IllegalArgumentException("Only cards that are part of a currently available pool can be drawn.")
    }

    val cheapCards: Map<Card<Establishment>, Int>
        get() = cheapCardPool.currentPool

    val expensiveCards: Map<Card<Establishment>, Int>
        get() = expensiveCardPool.currentPool

    val majorCards: Map<Card<Establishment>, Int>
        get() = majorCardPool.currentPool

    val landmarkCards: Set<Card<Landmark>>
        get() = landmarkCardPool.availableCards

    val allAvailableCards: Set<Card<Building>>
        get() = cardPools.flatMap { it.availableCards }.toSet()
}

private interface SingleCardPool {
    fun drawCard(card: Card<Building>): Card<Building>?

    val availableCards: Set<Card<Building>>
}

private class SingleInexhaustibleCardPool(val buildings: Set<Landmark>) : SingleCardPool {
    override fun drawCard(card: Card<Building>): Card<Building>? =
        buildings.find { it == card.building }?.let { Card(it) }

    override val availableCards: Set<Card<Landmark>>
        get() = buildings.map { Card(it) }.toSet()
}

private class SingleExhaustibleCardPool(val poolSize: Int, val stack: Stack<Card<Establishment>>) : SingleCardPool {

    private val _currentPool: MutableList<Card<Establishment>> = emptyList<Card<Establishment>>().toMutableList()
    val currentPool: Map<Card<Establishment>, Int>
        get() = _currentPool.groupingBy { it }.eachCount()

    init {
        shuffle()
        refillPool()
    }

    override fun drawCard(card: Card<Building>): Card<Building>? =
        _currentPool.find { it == card }.also {
            _currentPool.remove(it)
            refillPool()
        }

    override val availableCards: Set<Card<Building>>
        get() = currentPool.keys

    fun refillPool() {
        while (currentPool.size < poolSize && !stack.empty()) {
            _currentPool.add(stack.pop())
        }
    }

    fun shuffle() {
        val asList = stack.toMutableList()
        asList.shuffle()
        stack.removeAllElements()
        stack.addAll(asList)
    }
}

val standardCardPool: CardPool
    get() {
        val cheapStack = Stack<Card<Establishment>>()
        val expensiveStack = Stack<Card<Establishment>>()
        val majorStack = Stack<Card<Establishment>>()

        val allEstablishments = baseEstablishments + harborEstablishments + millionaireEstablishments

        repeat(6) {
            allEstablishments
                    .filterNot { it is MajorEstablishment }
                    .groupBy { it.activationTriggers.max() ?: 0 > 6 }
                    .forEach { (triggerAbove6, establishments) ->
                        if (triggerAbove6) {
                            expensiveStack += establishments.map { Card(it) }
                        } else {
                            cheapStack += establishments.map { Card(it) }
                        }
                    }
        }

        repeat(4) {
            majorStack += allEstablishments.filterIsInstance<MajorEstablishment>().map { Card(it) }
        }


        return CardPool(
                cheapPoolSize = 6,
                expensivePoolSize = 6,
                majorPoolSize = 2,
                cheapStack = cheapStack,
                expensiveStack = expensiveStack,
                majorStack = majorStack,
                availableLandmarks = ALL_LANDMARKS
        )
    }

val ALL_LANDMARKS: Set<Landmark> = setOf(
    cityHall,
    harbor,
    amusementPark,
    radioTower,
    shoppingMall,
    trainStation,
    airport
)

val baseEstablishments = listOf(
    wheatField,
    ranch,
    forest,
    mine,
    appleOrchard,
    bakery,
    convenienceStore,
    cheeseFactory,
    furnitureFactory,
    fruitAndVegetableMarket,
    cafe,
    stadium,
    tvStation,
    businessCenter
)

val harborEstablishments = listOf(
    flowerGarden,
    tunaBoat,
    mackerelBoat,
    flowerShop,
    foodWarehouse,
    sushiBar,
    pizzaJoint,
    hamburgerStand,
    publisher,
    taxOffice
)

val millionaireEstablishments = listOf(
    cornField,
    vineyard,
    generalStore,
    movingCompany,
    loanOffice,
    winery,
    sodaBottlingPlant,
    demolitionCompany,
    frenchRestaurant,
    membersOnlyClub,
    park,
    renovationCompany,
    techStartup,
    conventionCenter
)
