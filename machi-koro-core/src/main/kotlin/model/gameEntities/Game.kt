package model.gameEntities

import model.Card
import model.buildings.Building
import model.buildings.effects.buffs.*
import model.buildings.effects.establishmentEffects.EffectEvents
import model.buildings.establishments.Establishment
import model.buildings.establishments.MajorEstablishment
import model.cards.millionaire.conventionCenter

data class GameEvents(
    var onNextRound: (roundNumber: Int, players: List<Player>) -> Unit = { _, _ -> },
    var onNextPlayer: (player: Player) -> Unit = {},
    var onDiceRoll: (rollResult: List<Int>) -> Unit = {},
    var onFinalRoll: (value: Int) -> Unit = {},
    var onReRoll: (player: Player) -> Unit = {},
    var onRollIncrease: (player: Player) -> Unit = {},
    var onAdditionalRoll: (player: Player) -> Unit = {},
    var onPurchase: (player: Player, building: Building) -> Unit = { _, _ -> },
    var onPurchaseError: (type: Game.PurchaseEligibility) -> Unit = {},
    var onEffectTrigger: (activePlayer: Player, owner: Player, establishment: Establishment) -> Unit = { _, _, _ -> },
    var onEffectStatusUpdate: (message: String) -> Unit = {},
    var onGameEnd: (winners: List<Player>) -> Unit = {},
    var afterEffects: (currentPlayer: Player, players: List<Player>) -> Unit = { _, _ -> },
    var onFreeCoin: (player: Player) -> Unit = {},
    var onCoinsForPassing: (player: Player, amount: Int) -> Unit = { _, _ -> }
)

class Game(val players: List<Player>, val cardPool: CardPool, events: GameEvents? = null) {
    var turn = 0
        private set

    val events: GameEvents = events ?: GameEvents()

    private var lastAdditionalRollInfo = object {
        var turn: Pair<Player, Int>? = null
        var result: Int = 0
    }

    fun performRound() {
        turn++
        events.onNextRound(turn, players)
        players.forEach { singleTurn(it) }
        winners.takeIf { it.isNotEmpty() }?.let { events.onGameEnd(it) }
    }

    val winners: List<Player> = this.players.filter { it.landmarks.containsAll(ALL_LANDMARKS) }

    val isRunning = winners.isEmpty()

    private fun singleTurn(player: Player) {
        var doAdditionalTurn = false

        events.onNextPlayer(player)

        val rollResult = performDiceRolling(player) { events.onAdditionalRoll(player).also { doAdditionalTurn = true } }
        events.onFinalRoll(rollResult)

        triggerEffects(rollResult, player)

        events.afterEffects(player, players)

        grantFreeCoinIfNeeded(player)

        purchaseBuildingForPlayer(player)

        askPlayerForInvestment(player)

        if (doAdditionalTurn) {
            singleTurn(player)
        }
    }

    private fun askPlayerForInvestment(player: Player) {
        player.cards
            .filter { it.building.buff is InvestmentBuff }
            .forEach {
                if (player.money > 0 && player.askForInvestment(1, it)) {
                    player.money -= 1
                    it.moneyInvested += 1
                }
            }
    }

    private fun performDiceRolling(player: Player, onAdditionalRoll: () -> Unit): Int {
        var firstReRoll = false
        var secondReRoll = false
        val firstResult = askPlayerToRoll(player) { firstReRoll = true }
        if (player.hasBuff<ReRollBuff>() && player.chooseReroll()) {
            events.onReRoll(player)
            val secondResult = askPlayerToRoll(player) { secondReRoll = true }
            if (secondReRoll) {
                onAdditionalRoll()
            }
            return secondResult.sum()
        }
        if (firstReRoll) {
            onAdditionalRoll()
        }
        return firstResult.sum()
    }

    private fun askPlayerToRoll(player: Player, onDoubleRoll: () -> Unit): List<Int> =
        rollDice(if (player.hasBuff<DoubleDiceBuff>()) player.chooseDice() else DiceCount.ONE)
            .also { rollResult ->
                if (rollResult.groupingBy { it }.eachCount().containsValue(2) && player.hasBuff<AdditionalRollBuff>()) {
                    onDoubleRoll()
                }
            }
            .let { rollResult ->
                events.onDiceRoll(rollResult)
                if (player.hasBuff<AddTwoToRollBuff>() && rollResult.sum() >= 10 && player.increaseRoll()) {
                    events.onRollIncrease(player)
                    rollResult + 2
                } else rollResult
            }

    private fun grantFreeCoinIfNeeded(player: Player) {
        if (player.hasBuff<FreeCoinBuff>() && player.money == 0) {
            player.money = 1
            events.onFreeCoin(player)
        }
    }

    private fun triggerEffects(rollResult: Int, activePlayer: Player) {
        players
            .flatMap { player ->
                player.establishmentCards.map { card ->
                    player to card
                }
            }
            .sortedBy { (_, card) -> card.building.effect.priority }
            .filter { (player, card) ->
                card.building.activationTriggers.contains(rollResult) && card.building.willTrigger(
                    player == activePlayer,
                    activePlayer,
                    player
                )
            }
            .forEach { (player, card) ->
                events.onEffectTrigger(activePlayer, player, card.building)
                lateinit var effectEvents: EffectEvents
                effectEvents= EffectEvents(
                    onTextOutput = {
                        events.onEffectStatusUpdate(it)
                    },
                    onDiceRollRequest = { diceCount ->
                        if (lastAdditionalRollInfo.turn == activePlayer to turn) {
                            lastAdditionalRollInfo.result
                        } else {
                            lastAdditionalRollInfo.turn = activePlayer to turn
                            rollDice(diceCount)
                                .also { events.onDiceRoll(it) }
                                .sum()
                                .also { lastAdditionalRollInfo.result = it }
                        }
                    },
                    onConventionCenterEffect = { establishment ->
                        activePlayer.cards.remove(card)
                        activePlayer.establishments.filter { it == establishment }.forEach {
                            events.onEffectTrigger(activePlayer, player, it)
                            it.effect.apply(player, activePlayer, players.filter { it !== player }, card, effectEvents)
                        }
                    })

                card.building.effect.apply(player, activePlayer, players.filter { it !== player }, card, effectEvents)
            }
    }

    private fun purchaseBuildingForPlayer(player: Player) {
        var card: Card<Building>?
        var purchaseEligibility: PurchaseEligibility

        do {
            card = player.selectCardForPurchase(cardPool)
            purchaseEligibility = isEligibleForPurchase(player, card?.building)
            if (purchaseEligibility !== PurchaseEligibility.ELIGIBLE) {
                events.onPurchaseError(purchaseEligibility)
            }
        } while (purchaseEligibility != PurchaseEligibility.ELIGIBLE)

        if (card != null) {
            cardPool.drawCard(card)

            player.money -= card.building.cost
            player.addCard(card)

            events.onPurchase(player, card.building)
        } else if (player.hasBuff<FreeCoinsForPassingBuff>()) {
            val amount = (player.activeBuffs.find { it is FreeCoinsForPassingBuff } as FreeCoinsForPassingBuff).amount
            player.money += amount
            events.onCoinsForPassing(player, amount)
        }
    }

    private fun isEligibleForPurchase(player: Player, building: Building?): PurchaseEligibility {
        return when {
            building == null -> PurchaseEligibility.ELIGIBLE
            player.money < building.cost -> PurchaseEligibility.NO_MONEY
            player.establishments
                .filterIsInstance<MajorEstablishment>()
                .contains(building) -> PurchaseEligibility.MAJOR_ESTABLISHMENTS_MUST_BE_UNIQUE
            player.landmarks
                .contains(building) -> PurchaseEligibility.LANDMARKS_MUST_BE_UNIQUE
            else -> PurchaseEligibility.ELIGIBLE
        }

    }

    enum class PurchaseEligibility {
        ELIGIBLE, NO_MONEY, MAJOR_ESTABLISHMENTS_MUST_BE_UNIQUE, LANDMARKS_MUST_BE_UNIQUE
    }
}

private fun rollDice(diceCount: DiceCount): List<Int> =
    when (diceCount) {
        DiceCount.ONE -> listOf(rollWithOne())
        DiceCount.TWO -> rollWithTwo().toList()
        DiceCount.LEET -> listOf(rollWith1337())
    }
