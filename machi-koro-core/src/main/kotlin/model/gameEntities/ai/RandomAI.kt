package model.gameEntities.ai

import model.Card
import model.buildings.Building
import model.buildings.establishments.MajorEstablishment
import model.gameEntities.CardPool
import model.gameEntities.Player
import model.gameEntities.DiceCount
import java.util.*

class RandomAI(name: String) : Player(name) {
    override fun chooseDice(): DiceCount = setOf(DiceCount.ONE, DiceCount.TWO).random()

    override fun chooseReroll(): Boolean = Random().nextBoolean()
    override fun increaseRoll(): Boolean = Random().nextBoolean()

    @ExperimentalStdlibApi
    override fun selectCardForPurchase(cardPool: CardPool): Card<Building>? =
        cardPool.allAvailableCards
            .filter { it.building.cost <= money }
            .filter { it.building !in landmarks }
            .filter { it.building !in establishments.filterIsInstance<MajorEstablishment>() }
            .randomOrNull()

    override fun choosePlayer(players: List<Player>, customText: String?): Player = players.random()

    override fun chooseCard(cards: List<Card<Building>>, customText: String?): Card<Building> = cards.random()
    override fun chooseBuilding(buildings: List<Building>, customText: String?): Building = buildings.random()
    override fun askForInvestment(amount: Int, card: Card<Building>): Boolean = Random().nextBoolean()

}