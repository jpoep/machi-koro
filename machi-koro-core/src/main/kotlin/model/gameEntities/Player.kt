package model.gameEntities

import model.Card
import model.buildings.Building
import model.buildings.Landmark
import model.buildings.effects.buffs.Buff
import model.buildings.establishments.Establishment
import model.cards.base.bakery
import model.cards.base.cafe
import model.cards.base.wheatField
import model.cards.harbor.cityHall
import model.cards.millionaire.conventionCenter
import model.cards.millionaire.renovationCompany
import model.cards.millionaire.techStartup
import kotlin.random.Random

abstract class Player(
    val name: String = Random.nextInt().toString(),
    initialMoney: Int = 3,
    initialCards: List<Card<Building>> = listOf(
        wheatField,
        bakery,
        cityHall
    ).map { Card(it) }
) {

    val cards: MutableList<Card<Building>> = initialCards.toMutableList()

    var moneyLost: Int = 0
        private set

    var moneyGained: Int = 0
        private set

    var money: Int = initialMoney
        set(value) {
            val correctedValue = if (value < 0) 0 else value
            val difference = correctedValue - money
            if (difference < 0) {
                moneyLost -= difference
            } else {
                moneyGained += difference
            }
            field = correctedValue
        }

    val buildings: List<Building>
        get() = cards.map { it.building }

    val establishmentCards: List<Card<Establishment>>
        get() = cards.filter { it.building is Establishment }.map { it as Card<Establishment> }

    val establishments: List<Establishment>
        get() = buildings.filterIsInstance<Establishment>()

    val landmarkCards: List<Card<Landmark>>
        get() = cards.filter { it.building is Landmark }.map { it as Card<Landmark> }

    val landmarks: List<Landmark>
        get() = buildings.filterIsInstance<Landmark>()

    val constructedLandmarkCards: List<Card<Landmark>>
        get() = landmarkCards.filterNot { it.building == cityHall }

    val constructedLandmarks: List<Landmark>
        get() = landmarks.filterNot { it == cityHall }

    val activeBuffs: List<Buff>
        get() = buildings.mapNotNull { it.buff }

    inline fun <reified T> hasBuff(): Boolean = activeBuffs.filterIsInstance<T>().isNotEmpty()

    fun addCard(card: Card<Building>) {
        cards.add(card)
    }

    abstract fun chooseDice(): DiceCount

    abstract fun chooseReroll(): Boolean

    abstract fun increaseRoll(): Boolean

    abstract fun selectCardForPurchase(cardPool: CardPool): Card<Building>?

    abstract fun choosePlayer(players: List<Player>, customText: String? = null): Player

    abstract fun chooseCard(cards: List<Card<Building>>, customText: String? = null): Card<Building>

    abstract fun chooseBuilding(buildings: List<Building>, customText: String? = null): Building

    abstract fun askForInvestment(amount: Int = 1, card: Card<Building>): Boolean

}