import model.asCoins
import model.buildings.establishments.Establishment
import model.gameEntities.Game
import model.gameEntities.GameEvents
import model.gameEntities.Player
import model.gameEntities.standardCardPool


class HumanPlayerRunner(players: List<Player>) {

    private var gameRunning = false

    private val events = GameEvents(
        onDiceRoll = {
            println("""A $it was rolled!""")
        },
        onEffectTrigger = { _, owner, establishment ->
            println("${owner.name}'s ${establishment.name} activated:")
        },
        onPurchaseError = {
            print("This building cannot be bought because ")
            println(
                when (it) {
                    Game.PurchaseEligibility.MAJOR_ESTABLISHMENTS_MUST_BE_UNIQUE -> "you can only have one of each major establishment."
                    Game.PurchaseEligibility.NO_MONEY -> "you don't have enough money."
                    Game.PurchaseEligibility.LANDMARKS_MUST_BE_UNIQUE -> "you have already built this landmark."
                    else -> "This should literally never happen. If you see this text, tell Jonas about it. Hopefully, he'll remember."
                }
            )
        },
        onGameEnd = { winners ->
            winners.forEach {
                println("${it.name} won after ${game.turn} rounds!")
            }
            printFinalStats(game, winners)
            gameRunning = false
        },
        onNextPlayer = {
            println("-----------------------------------------------------")
            println("""${it.name}'s turn.""")
            println("-----------------------------------------------------")
        },
        onNextRound = { round, players ->
            println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            println("+--------------------------------------------------------------------------------------------------------+")
            println("+--------------------------------------------------------------------------------------------------------+")
            println("+--------------------------------------------------------------------------------------------------------+")
            println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            println("Round $round begins. Here are your moneys:")
            players.forEach {
                println("""${it.name}: ${it.money}""")
            }
        },
        onPurchase = { player, building ->
            println("""${player.name} purchased ${building.name} for ${building.cost}""")
        },
        afterEffects = { currentPlayer, players ->
            println("""Here are the new moneys:""")
            players.forEach {
                println("""${it.name}: ${it.money}""")
            }
            println("${currentPlayer.name} may purchase now.")
        },
        onFreeCoin = {
            println("${it.name} was so poor at the start of their turn that they received a free coin.")

        },
        onAdditionalRoll = {
            println("${it.name} rolled doubles and may roll again.")
        },
        onCoinsForPassing = { player, amount ->
            println("${player.name}'s Airport causes them to receive $amount coins for passing their turn.")
        },
        onFinalRoll = {
            println("The total roll is $it.")
        },
        onReRoll = {
            println("${it.name} chose to re-roll.")
        },
        onRollIncrease = {
            println("${it.name} increased their roll by 2.")
        },
        onEffectStatusUpdate = {
            println("\t$it")
        }
    )

    private val game: Game = Game(players, standardCardPool, events)

    fun run() {
        gameRunning = true
        while (gameRunning) {
            game.performRound()
        }
    }

    private fun printFinalStats(game: Game, winners: List<Player>) {
        println("------------------------Final Stats: ---------------------------")
        game.players.forEach { player ->
            println("${player.name}:")

            player.buildings
                .sortedBy {
                    when (it is Establishment) {
                        true -> it.activationTriggers.average().toInt()
                        false -> 0
                    }
                }
                .groupBy { it.javaClass.kotlin }
                .forEach { (_, buildings) ->
                    println(
                        buildings
                            .groupingBy { it }
                            .eachCount()
                            .entries
                            .map { (building, count) -> "${count}x ${building.name} ${if (building is Establishment) "${building.activationTriggers} " else ""}for a value of ${(building.cost * count).asCoins}\n" }
                            .reduce { acc, s -> acc + s }
                    )
                }

            println("----------------------------------------------")
            println("Total value of buildings: ${player.buildings.map { it.cost }.reduce { acc, i -> acc + i }}")
            println("Total money gained: ${player.moneyGained}")
            println("Total money spent/lost: ${player.moneyLost}")
            println("----------------------------------------------")
            println()
        }
        println("Turns taken: ${game.turn}")
        println("Winners: ${winners.map { it.name }}")
    }
}
