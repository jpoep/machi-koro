import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import model.gameEntities.*
import model.gameEntities.ai.*
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

val ALL_AIs
    get() = listOf(
        { RandomAI("") },
        { CheapestAI("") },
        { MostExpensiveAI("") },
        { PrimaryBuildingAI("") },
        { SecondaryBuildingAI("") },
        { RestaurantAI("") },
        { MajorBuildingAI("") }
    )

@ExperimentalTime
fun testSet(playerGenerator: List<() -> Player>, playerAmount: IntRange, repetitions: Int) {
    playerGenerator.forEach { ai ->
        playerAmount.forEach { amount ->
            testWith(ai, amount, repetitions)
        }
    }
}

data class Stats(
    val turns: MutableList<Int>,
    val winnerMoneyGained: MutableList<Int>,
    val winnerBuildingNetWorth: MutableList<Int>
)

val statsContext = newSingleThreadContext("StatsContext")
val mutex = Mutex()

@ExperimentalTime
fun testWith(playerGenerator: () -> Player, playerAmount: Int, repetitions: Int) {
    val stats = Stats(mutableListOf(), mutableListOf(), mutableListOf())
    val name = playerGenerator()::class.simpleName

    println("----------------------------------------------------------------${playerAmount}x $name--------------------------------------------------------------------")
    print("Playing game")
    val time = measureTime {
        runBlocking {
            withContext(Dispatchers.Default) {
                repeat(repetitions) {
                    launch {
                        singleRepetition(stats, playerAmount, playerGenerator, repetitions)
                    }
                }
            }
        }
    }

    println(" (finished after ${time})")
    println("Stats for $name ($repetitions games, $playerAmount players):")
    println("Rounds taken (avg/min/max): ${stats.turns.average()}/${stats.turns.min()}/${stats.turns.max()}")
    println("Winner money gained (avg/min/max): ${stats.winnerMoneyGained.average()}/${stats.winnerMoneyGained.min()}/${stats.winnerMoneyGained.max()}")
    println("Winner building net worth (avg/min/max): ${stats.winnerBuildingNetWorth.average()}/${stats.winnerBuildingNetWorth.min()}/${stats.winnerBuildingNetWorth.max()}")
}

suspend fun singleRepetition(stats: Stats, playerAmount: Int, playerGenerator: () -> Player, repetitions: Int) {
    val testors = (0..playerAmount).map { playerGenerator() }.toList()
    var gameWon = false
    val game = Game(testors, standardCardPool).apply {
        events.onGameEnd = {
            gameWon = true
        }
    }
    while (!gameWon) {
        game.performRound()
    }

    mutex.withLock {
        stats.turns.add(game.turn)
        stats.winnerMoneyGained.add(game.players.first { it.landmarks.containsAll(ALL_LANDMARKS) }.moneyGained)
        stats.winnerBuildingNetWorth.add(game.players.first { it.landmarks.containsAll(ALL_LANDMARKS) }.buildings.map { it.cost }
            .sum())
        stats.turns.count().takeIf { it % (repetitions / 20) == 0 }?.let { print("...$it") }
    }
}

