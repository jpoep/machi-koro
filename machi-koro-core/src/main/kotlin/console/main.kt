package console

import ArgumentsParser
import kotlin.time.ExperimentalTime


@ExperimentalTime
fun main(args: Array<String>) {
    ArgumentsParser(args.toList()).run()
}
