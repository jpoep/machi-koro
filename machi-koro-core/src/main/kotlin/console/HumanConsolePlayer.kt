package console

import model.Card
import model.asCoins
import model.buildings.Building
import model.gameEntities.CardPool
import model.gameEntities.Player
import model.gameEntities.DiceCount
import java.util.*

class HumanConsolePlayer(name: String) : Player(name) {
    override fun chooseDice(): DiceCount {
        println("Roll with one or two dice?")

        val scanner = Scanner(System.`in`)
        var input = scanner.nextLine()

        val returnMap = mapOf(
            "1" to DiceCount.ONE,
            "2" to DiceCount.TWO,
            "one" to DiceCount.ONE,
            "two" to DiceCount.TWO,
            "1337" to DiceCount.LEET
        )

        while (!returnMap.keys.contains(input)) {
            println("Dude, no.")
            input = scanner.next()
        }

        return returnMap[input]!!
    }

    override fun chooseReroll(): Boolean {
        println("Wanna re-roll? (y/n)")

        val scanner = Scanner(System.`in`)
        var input = scanner.nextLine()

        val returnMap = mapOf(
            "y" to true,
            "yes" to true,
            "n" to false,
            "no" to false
        )

        while (!returnMap.keys.contains(input)) {
            input = scanner.next()
        }

        return returnMap[input]!!
    }

    override fun increaseRoll(): Boolean {
        println("Increase your roll by 2? (y/n)")

        val scanner = Scanner(System.`in`)
        var input = scanner.nextLine()

        val returnMap = mapOf(
            "y" to true,
            "yes" to true,
            "n" to false,
            "no" to false
        )

        while (!returnMap.keys.contains(input)) {
            input = scanner.next()
        }

        return returnMap[input]!!
    }

    override fun selectCardForPurchase(cardPool: CardPool): Card<Building>? {
        println("  Current Cheap Pool: ")
        cardPool.cheapCards.entries.forEach {
            println("""     ($${it.key.building.cost}) - ${it.value}x ${it.key.building.name}: --- triggers at ${it.key.building.activationTriggers} --- ${it.key.building.type.displayName} --- ${it.key.building.effect.description}""")
        }

        println("  Current Expensive Pool:")
        cardPool.expensiveCards.entries.forEach {
            println("""     ($${it.key.building.cost}) - ${it.value}x ${it.key.building.name}: --- triggers at ${it.key.building.activationTriggers} --- ${it.key.building.type.displayName} --- ${it.key.building.effect.description}""")
        }

        println("  Current Major Pool:")
        cardPool.majorCards.entries.forEach {
            println("""     ($${it.key.building.cost}) - ${it.value}x ${it.key.building.name}: --- triggers at ${it.key.building.activationTriggers} --- ${it.key.building.type.displayName} --- ${it.key.building.effect.description}""")
        }

        println("  Unbuilt landmarks:")
        cardPool.landmarkCards.map { it.building }.filterNot { landmarks.contains(it) }.sortedBy { it.cost }.forEach {
            println("""     ($${it.cost}) - ${it.name}: --- ${it.buff?.description}""")
        }

        println("Type the name of the building you want to buy or 'nothing' if you don't want to buy anything.")

        val scanner = Scanner(System.`in`)
        var input = scanner.nextLine()

        val totalCardPool = (cardPool.allAvailableCards)

        while (totalCardPool.none { it.building.name == input || input == "nothing" }) {
            println("There is no such building, please type again.")
            input = scanner.next()
        }

        return totalCardPool.find { it.building.name == input }
    }

    override fun choosePlayer(players: List<Player>, customText: String?): Player {
        println(customText ?: "$name, choose a player out of ")
        players.forEach {
            println("""     ${it.name}""")
        }

        val scanner = Scanner(System.`in`)
        var input = scanner.nextLine()

        while (players.none { it.name == input }) {
            println("There is no such player, please type again.")
            input = scanner.nextLine()
        }

        return players.find { it.name == input }!!
    }

    override fun chooseBuilding(buildings: List<Building>, customText: String?): Building {
        println(customText ?: "$name, choose a building out of ")
        buildings
            .distinct()
            .forEach { println("\t${it.name}") }

        val scanner = Scanner(System.`in`)
        var input = scanner.nextLine()

        while (buildings.none { it.name == input }) {
            println("There is no such building, please type again.")
            input = scanner.nextLine()
        }

        return buildings.first { it.name == input }
    }

    override fun askForInvestment(amount: Int, card: Card<Building>): Boolean {
        println("Do you want to invest ${amount.asCoins} in your ${card.building.name}? (y/n)")
        val scanner = Scanner(System.`in`)
        var input = scanner.nextLine()

        val returnMap = mapOf(
            "y" to true,
            "yes" to true,
            "n" to false,
            "no" to false
        )

        while (!returnMap.keys.contains(input)) {
            input = scanner.next()
        }

        return returnMap[input]!!
    }

    override fun chooseCard(cards: List<Card<Building>>, customText: String?): Card<Building> {
        println(customText ?: "$name, choose a card out of ")
        cards
            .distinct()
            .forEach { println("\t${it.building.name} ${if (it.closedForRenovation) "(currently closed for renovation!)" else ""}") }

        val scanner = Scanner(System.`in`)
        var input = scanner.nextLine()

        while (cards.none { it.building.name == input }) {
            println("There is no such building, please type again.")
            input = scanner.nextLine()
        }

        return cards.first { it.building.name == input }
    }

}