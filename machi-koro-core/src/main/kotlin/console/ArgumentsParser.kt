import console.HumanConsolePlayer
import model.gameEntities.Player
import model.gameEntities.ai.RandomAI
import kotlin.time.ExperimentalTime

class ArgumentsParser(args: List<String>) {

    private val HELP_MESSAGE =
    """To play a game, specify how many players are playing. Use it like so: --players Player1 Player2 --ai Ai1 Ai2 Ai3
        |Here's a list of all available options: 
        |${listOf(playersFlag, aiFlag, testFlag).map { "\t--${it.flag}: ${it.description}\n" }.reduce {acc, s -> acc + s }}
    """.trimMargin()
    private val arguments: List<Argument> = extractArguments(args)

    @ExperimentalTime
    fun run() {
        val gameType = when (arguments.map { it.arg }.contains(testFlag.flag)) {
            true -> GameType.AI_TEST
            false -> GameType.HUMAN
        }

        when (gameType) {
            GameType.HUMAN -> {
                val humans =
                    arguments.findLast { it.arg == playersFlag.flag || it.arg == playersFlag.shortFlag.toString() }
                        ?.let {
                            playersFlag.parse(it)
                        } ?: listOf()
                val ais =
                    arguments.findLast { it.arg == aiFlag.flag || it.arg == aiFlag.shortFlag.toString() }
                        ?.let {
                            aiFlag.parse(it)
                        } ?: listOf()

                if ((humans + ais).isEmpty()) {
                    println(HELP_MESSAGE)
                } else {
                    HumanPlayerRunner(humans + ais).run()
                }

            }
            GameType.AI_TEST -> {
                val params =
                    testFlag.parse(arguments.findLast { it.arg == testFlag.flag || it.arg == testFlag.shortFlag.toString() }!!)
                testSet(params.players, params.playerCount, params.reps)
            }
        }
    }

    enum class GameType {
        HUMAN, AI_TEST
    }
}

private fun extractArguments(args: List<String>): List<Argument> {
    var currentArgument: Argument? = null
    val allArguments = mutableListOf<Argument>()
    args.map { it.trim() }
        .filter { it != "" }
        .forEach { arg ->
            when {
                arg.startsWith("--") -> {
                    currentArgument = Argument(arg.removePrefix("--"))
                    allArguments.add(currentArgument!!)
                }
                arg.startsWith("-") -> {
                    allArguments.addAll(arg.removePrefix("-").chunked(1).map { Argument(it) })
                    currentArgument = allArguments.last()
                }
                else -> {
                    currentArgument?.params?.add(arg)
                }
            }
        }

    return allArguments
}

data class Argument(val arg: String, val params: MutableList<String> = mutableListOf())

interface Flag<T> {
    val flag: String
    val shortFlag: Char
    val description: String
    fun parse(arg: Argument): T
}

val playersFlag = object : Flag<List<HumanConsolePlayer>> {
    override val flag: String
        get() = "players"

    override val shortFlag: Char
        get() = 'p'

    override val description: String
        get() = "Specify how many human players want to play. Use it like '--players Player1 Player2 Player3' to start a game with three human players named Player1, Player2 and Player3."

    override fun parse(arg: Argument): List<HumanConsolePlayer> = arg.params.map { HumanConsolePlayer(it) }
}

val aiFlag = object : Flag<List<Player>> {
    override val flag: String
        get() = "ai"

    override val shortFlag: Char
        get() = 'a'

    override val description: String
        get() = "Specify how many AIs want to play. Use it like '--ai Ai1 Ai2 Ai3' to start a game with three ais named Ai1, Ai2 and Ai3. The AI strategy will be random."

    override fun parse(arg: Argument): List<Player> = arg.params.map { RandomAI(it) }
}

data class AiTestSuiteParams(val players: List<() -> Player>, val playerCount: IntRange, val reps: Int)

val testFlag = object : Flag<AiTestSuiteParams> {
    override val flag: String
        get() = "test"

    override val shortFlag: Char
        get() = 't'

    override val description: String
        get() = """Specifies that you want to run tests rather than play a game. Usage is as follows: --test reps={reps} players={player range}
            |            The example --test reps=1000 players=2..4 will execute 1000 games with 2 to 4 players for every AI that is currently available.            |   
        """.trimMargin()

    override fun parse(arg: Argument): AiTestSuiteParams {
        val players = ALL_AIs
        var reps = 1000
        var playerCount: IntRange = 2..3

        arg.params.forEach {
            val splitParam = it.split("=")
            val (key, value) = splitParam[0] to splitParam[1]
            when (key) {
                "reps" -> reps = value.toInt()
                "players" -> {
                    val rangeBounds = value.split("..").map { it.toInt() }
                    playerCount = rangeBounds[0]..rangeBounds[1]
                }
            }
        }

        return AiTestSuiteParams(players, playerCount, reps)
    }
}