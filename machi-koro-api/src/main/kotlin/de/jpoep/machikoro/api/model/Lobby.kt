package de.jpoep.machikoro.api.model

import model.gameEntities.Game
import model.gameEntities.GameEvents
import model.gameEntities.standardCardPool
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Lobby(val users: List<User>,
        val games: MutableList<Game>,
        @Id val uid: String) {

    val isGameRunning = games.last().isRunning

    fun start(events: GameEvents) {
        if (!isGameRunning) return
        games += Game(players = users.map { NetworkPlayer(it) },
                cardPool = standardCardPool,
                events = events)
    }
}