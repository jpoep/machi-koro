package de.jpoep.machikoro.api

import de.jpoep.machikoro.api.model.User
import kotlinx.coroutines.runBlocking
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.data.mongodb.core.ReactiveMongoOperations

@SpringBootApplication
class MachiKoroApiApplication {
    @Bean
    fun init(operations: ReactiveMongoOperations) = CommandLineRunner {
        runBlocking {
            operations
                    .insert(User("test", "testboi"))
                    .subscribe(System.out::println)
        }
    }
}

fun main(args: Array<String>) {
    runApplication<MachiKoroApiApplication>(*args)
}
