package de.jpoep.machikoro.api.test

import de.jpoep.machikoro.api.model.User
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository

//class TestRepository(private val db: MongoDatabase) {
//
//    suspend fun count(): Long =
//            client.execute().sql("SELECT COUNT(*) FROM users")
//                    .asType<Long>().fetch().awaitOne()
//
//    fun findAll(): Flow<User> =
//            client.select().from("users").asType<User>().fetch().flow()
//
//    suspend fun findOne(id: String): User? =
//            db.
//            client.execute()
//                    .sql("SELECT * FROM users WHERE login = :login")
//                    .bind("login", id).asType<User>()
//                    .fetch()
//                    .awaitOneOrNull()
//
//    suspend fun deleteAll() =
//            client.execute().sql("DELETE FROM users").await()
//
//    suspend fun save(user: User) =
//            client.insert().into<User>().table("users").using(user).await()
//
//    suspend fun init() {
//        client.execute().sql("CREATE TABLE IF NOT EXISTS users (login varchar PRIMARY KEY, firstname varchar, lastname varchar);").await()
//        deleteAll()
//        save(User("smaldini", "Stéphane", "Maldini"))
//        save(User("sdeleuze", "Sébastien", "Deleuze"))
//        save(User("bclozel", "Brian", "Clozel"))
//    }
//}

@Repository
interface TestRepository : ReactiveMongoRepository<User, String>