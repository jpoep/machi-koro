package de.jpoep.machikoro.api.test

import de.jpoep.machikoro.api.model.User
import de.jpoep.machikoroapi.model.User
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.router
import java.util.*

@Configuration
class TestRouter(private val testHandler: TestHandler) {

    @Bean
    fun plsWork() = router {
        GET("/save", testHandler::saveAndSend)
        GET("/all", testHandler::getAll)
        GET("/by-id/{id}", testHandler::find)
    }
}

@Component
class TestHandler(private val testRepository: TestRepository) {
    fun saveAndSend(req: ServerRequest) = ok().body(thefuq())
    fun getAll(req: ServerRequest) = ok().body(testRepository.findAll())
    fun find(req: ServerRequest) = ok().body(testRepository.findById(req.pathVariable("id")))

    private fun thefuq() =
            testRepository.save(User(UUID.randomUUID().toString(), "test"))

    private fun thefuq2() =
            testRepository.findById("kek")
}