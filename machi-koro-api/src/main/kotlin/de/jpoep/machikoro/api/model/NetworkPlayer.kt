package de.jpoep.machikoro.api.model

import model.Card
import model.buildings.Building
import model.gameEntities.CardPool
import model.gameEntities.DiceCount
import model.gameEntities.Player

class NetworkPlayer(user: User) : Player(user.nickName) {


    override fun chooseDice(): DiceCount {
        TODO("Not yet implemented")
    }

    override fun chooseReroll(): Boolean {
        TODO("Not yet implemented")
    }

    override fun increaseRoll(): Boolean {
        TODO("Not yet implemented")
    }

    override fun selectCardForPurchase(cardPool: CardPool): Card<Building>? {
        TODO("Not yet implemented")
    }

    override fun choosePlayer(players: List<Player>, customText: String?): Player {
        TODO("Not yet implemented")
    }

    override fun chooseCard(cards: List<Card<Building>>, customText: String?): Card<Building> {
        TODO("Not yet implemented")
    }

    override fun chooseBuilding(buildings: List<Building>, customText: String?): Building {
        TODO("Not yet implemented")
    }

    override fun askForInvestment(amount: Int, card: Card<Building>): Boolean {
        TODO("Not yet implemented")
    }
}